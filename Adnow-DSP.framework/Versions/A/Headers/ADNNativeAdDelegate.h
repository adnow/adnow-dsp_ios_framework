//
//  ADNNativeAdDelegate.h
//  Adnow
//
//  Copyright (c) 2015 Adnow. All rights reserved.
//

/**
 * The delegate of an `ADNativeAd` object must adopt the `ADNativeAdDelegate` protocol. It must
 * implement `viewControllerForPresentingModalView` to provide a root view controller from which
 * the ad view should present modal content.
 */

@protocol ADNNativeAdDelegate <NSObject>

@required

/** @name Managing Modal Content Presentation */

/**
 * Asks the delegate for a view controller to use for presenting modal content, such as the in-app
 * browser that can appear when an ad is tapped.
 *
 * @return A view controller that should be used for presenting modal content.
 */
- (UIViewController *)viewControllerForPresentingModalView;

@end
