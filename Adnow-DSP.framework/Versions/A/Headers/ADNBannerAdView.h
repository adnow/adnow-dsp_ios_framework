//
//  ADNAdView.h
//  Adnow-DSP
//
//  Created by Mokrane Mimouni on 25/01/2015.
//  Copyright (c) 2015 Adnow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ADNAdView.h"

@interface ADNBannerAdView : ADNAdView

- (id)initWithAdUnitId:(NSString *)editorId_ size:(CGRect)size;
- (id)initWithAdUnitId:(NSString *)editorId_ size:(CGRect)size andLocation:(CLLocation *) clientLocation;
- (void)loadAd;
- (void)updateLocation:(CLLocation *)location;
- (void)closeAd;
- (void)putKeywords:(NSString *) keywords;
@end
