//
//  ADNTimer.h
//  Adnow

//  Created by Mokrane Mimouni on 25/01/2015.
//  Copyright (c) 2015 Adnow. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * ADNTimer wraps an NSTimer and adds pause/resume functionality.
 */
@interface ADNTimer : NSObject

@property (nonatomic, copy) NSString *runLoopMode;

+ (ADNTimer *)timerWithTimeInterval:(NSTimeInterval)seconds
                            target:(id)target
                          selector:(SEL)aSelector
                           repeats:(BOOL)repeats;

- (BOOL)isValid;
- (void)invalidate;
- (BOOL)isScheduled;
- (BOOL)scheduleNow;
- (BOOL)pause;
- (BOOL)resume;
- (NSTimeInterval)initialTimeInterval;

@end
