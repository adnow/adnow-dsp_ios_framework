# AdNow - iOS Framework 1.2

## CONTACT
This documentation explains how the Adnow iOS SDK should be implemented. Before each integration please contact media@adnow.fr to obtain your placement ids.

Any questions please contact : it@adnow.fr.

## INSTALLATION/REQUIREMENTS

The iOS SDK can be used only with i386, arm64, armv7 and armv7s architectures and works for iOS version prior to 6.0.

Add the following frameworks to your project. To do this, click on your application's target, then click on Build Phases and expand the Link Binary With Libraries group. The Adnow iOS SDK requires the following frameworks :

* CoreTelephony

* AdSupport

* SystemConfiguration

* CoreLocation

* Foundation

* UIKit

In addition, this files are join to the framework and should be add to the project. To do this, click on your application’s target, then click on Build Phases and expand the Copy Bundle Ressources group:

* ADN.bundle

* ADNAdBrowserController.xib

Only Adnow's developers can update that files.

## IMPLEMENTATION
The Adnow SDK allows developers to choose between two kinds of Ads: Banner and Interstitial.
The banner’s position can be customize by editors.
Banner and interstitial display can be initialized anytime editors want.

### FOREGROUND AND BACKGROUND MANAGEMENT

Foreground and Background application management should inform Banner and Interstitial view to stop getting a new element.
Just set 

```
#!objective-c
ADNViewObject.hidden = NO;

```

when we want to continue to get a new element, set:

```
#!objective-c
ADNViewObject.hidden = NO; 
```

### BANNER ADS IMPLEMENTATION
```
#!objective-c

// MyViewController.h  

#import <Adnow-DSP/ADNBannerAdView.h>  

@implementation MyViewController : UIViewController <ADNDelegate>

@end

---------------------------------------------------------------------------------------

// MyViewController.m 
 
#import "MyViewController.h"  
@interface MyViewController ()
@property (nonatomic, strong) ADNBannerAdView *bannerView;

#define kEditorId @"YOUR_ADUNIT_ID_HERE"
@end

@implementation MyViewController
  
-	(void)viewDidLoad {   
// ... your other 
	self.bannerView = [[ADNBannerAdView alloc] initWithAdUnitId:kEditorId  
size:BANNER_SIZE];
// BANNER_SIZE field can be customize but should depends on kEditorId for the size but for the position it depends on client choice
	self.bannerView.delegate = self;
	self.bannerView.controller = self;

//when keywords is available, use putKeywords function to add extra information on banner
	[self.bannerView putKeywords:@"KEYWORDS,separated,by,comma"];

//when location is available, use updateLocation function to add location condition on banner
	[self.bannerView updateLocation:<CLLOCATION_OBJECT>];

	[self.view addSubview:self.bannerView];     
	[self.bannerView loadAd];   
// ... your other 
 
 	[super viewDidLoad];
 }  

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // to stop loading banner each X seconds
    [self.bannerView closeAd];
}

#pragma mark - <ADNDelegate> 

// when banner failed to load
-	(void)didDisplayAdFail:(NSError *)error forId:(NSString *)editorId {
 // ... your code 
}

// when banner finish to load
-	(void)didDisplayAd:(NSString *)editorId {
// ... your code 
}

// when click raise on banner
-	(void)clickView:(NSString *)editorId {
// ... your code 
}

// when webpage closed
-	(void)closeWebPage:(NSString *)editorId {
// ... your code 
}

  ...
 @end

```

### INTERSTITIAL ADS IMPLEMENTATION

```
#!objective-c

// MyViewController.h  

#import <Adnow-DSP/ADNInterAdView.h>  

@implementation MyViewController : UIViewController <ADNDelegate>

@end

---------------------------------------------------------------------------------------


// MyViewController.m 
 
#import "MyViewController.h"  
@interface MyViewController ()
@property (nonatomic, strong) ADNInterAdView *interView;

#define kEditorId @"YOUR_ADUNIT_ID_HERE"
@end

@implementation MyViewController
  
-	(void)viewDidLoad {   
// ... your other 
	self.interView = [[ADNInterAdView alloc] initWithAdUnitId:kEditorId];
	self.interView.delegate = self;
	self.interView.controller = self;

//when keywords is available, use putKeywords function to add extra information on banner
	[self.interView putKeywords:@"KEYWORDS,separated,by,comma"];

//when location is available, use updateLocation function to add location condition on banner
	[self.interView updateLocation:<CLLOCATION_OBJECT>];

	[self.interView loadAd];   
// ... your other 
 
 	[super viewDidLoad];
 }  

#pragma mark - <ADNDelegate> 

// when banner failed to load
-	(void)didDisplayAdFail:(NSError *)error forId:(NSString *)editorId {
 // ... your code 
}

// when banner finish to load
-	(void)didDisplayAd:(NSString *)editorId {
// ... your code 
}

// when click raise on banner
-	(void)clickView:(NSString *)editorId {
// ... your code 
}

// when webpage closed
-	(void)closeWebPage:(NSString *)editorId {
// ... your code 
}

  ...
 @end

```
