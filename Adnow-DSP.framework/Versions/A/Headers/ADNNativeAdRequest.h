//
//  ADNNativeAdRequest.h
//  Copyright (c) 2015 Adnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADNNativeAd;
@class ADNNativeAdRequest;
@class ADNNativeAdRequestTargeting;

typedef void(^ADNNativeAdRequestHandler)(ADNNativeAdRequest *request,
                                      ADNNativeAd *response,
                                      NSError *error);

////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * The `ADNNativeAdRequest` class is used to manage individual requests to the Adnow ad server for
 * native ads.
 *
 * @warning **Note:** This class is meant for one-off requests for which you intend to manually
 * process the native ad response. If you are using `ADNTableViewAdPlacer` or
 * `ADNCollectionViewAdPlacer` to display ads, there should be no need for you to use this class.
 */

@interface ADNNativeAdRequest : NSObject

/** @name Targeting Information */

/**
 * An object representing targeting parameters that can be passed to the Adnow ad server to
 * serve more relevant advertising.
 */
@property (nonatomic, strong) ADNNativeAdRequestTargeting *targeting;

/** @name Initializing and Starting an Ad Request */

/**
 * Initializes a request object.
 *
 * @param identifier The ad unit identifier for this request. An ad unit is a defined placement in
 * your application set aside for advertising. Ad unit IDs are created on the Adnow website.
 *
 * @return An `ADNNativeAdRequest` object.
 */
+ (ADNNativeAdRequest *)requestWithAdUnitIdentifier:(NSString *)identifier;

/**
 * Executes a request to the Adnow ad server.
 *
 * @param handler A block to execute when the request finishes. The block includes as parameters the
 * request itself and either a valid ADNNativeAd or an NSError object indicating failure.
 */
- (void)startWithCompletionHandler:(ADNNativeAdRequestHandler)handler;

@end
