//
//  ADNAdView.h
//  Adnow-DSP
//
//  Created by Mokrane Mimouni on 25/01/2015.
//  Copyright (c) 2015 Adnow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ADNAdView.h"

@interface ADNInterAdView: NSObject
@property (nonatomic, strong) NSString *editorId;
@property (nonatomic, copy) CLLocation *location;
@property (nonatomic, strong) UIViewController *controller;
@property (nonatomic, weak) id<ADNDelegate> delegate;

/** @name Init new Interstitial Ads */

/**
 * Creates and returns an empty ADNInterAdView object.
 *
 * @return A newly initialized ADNNativeAdRequestTargeting object.
 */

- (id)initWithAdUnitId:(NSString *)editorId_;

/**
 */
- (id)initWithAdUnitId:(NSString *)editorId_ andLocation:(CLLocation *) clientLocation;

/**
 */
- (void) loadAd;

/**
 
 */
- (void)updateLocation:(CLLocation *) clientLocation;
/**
 
 */
- (void)putKeywords:(NSString *) keywords;

@end
