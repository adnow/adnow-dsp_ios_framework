//
//  ADNAdView.h
//  Adnow-DSP
//
//  Created by Mokrane Mimouni on 25/01/2015.
//  Copyright (c) 2015 Adnow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "ADNNativeAdDelegate.h"
#import "ADNNativeAdRendering.h"
#import "ADNNativeAd.h"
#import "ADNNativeAdRequest.h"
#import "ADNNativeAdRequestTargeting.h"
#import "ADNTimer.h"

typedef enum
{
    ADNNativeAdOrientationAny,
    ADNNativeAdOrientationPortrait,
    ADNNativeAdOrientationLandscape
} ADNNativeAdOrientation;


@protocol ADNDelegate <NSObject>
/** @name raise when ad image display */

/**
 * This callback trigered when the image display
 *
 * @param editorId id of the ads
 */
-(void)didDisplayAd:(NSString *)editorId;

/** @name raise when ad image display fail */

/**
 * This callback trigered when the image cannot display
 *
 * @param error information
 * @param editorId id of the ads
 */
-(void)didDisplayAdFail:(NSError *)error forId:(NSString *) editorId;

/** @name raise when user closes the webview page*/

/**
 * This callback trigered when user closes the webview manually
 *
 * @param editorId id of the ads
 */
-(void)closeWebPage:(NSString *)editorId;

/** @name raise when user clicks on ads */

/**
 * This callback trigered when user clicks on ads
 *
 * @param editorId id of the ads
 */
-(void)clickView:(NSString *)editorId;

@end

@interface ADNAdView : UIView <ADNNativeAdDelegate, ADNNativeAdRendering>
@property (nonatomic, weak) UIView *adContentView;
@property (nonatomic, assign) CGSize originalSize;
@property (nonatomic, assign) ADNNativeAdOrientation allowedNativeAdOrientation;
@property (nonatomic, copy) CLLocation *location;
@property (nonatomic, assign, getter = isTesting) BOOL testing;
@property (nonatomic, strong) NSString *editorId;
@property (nonatomic, strong) ADNNativeAd *nativeAd;
@property (nonatomic, strong) ADNNativeAdRequest *adRequest1;
@property (nonatomic, strong) ADNNativeAdRequestTargeting *targeting;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) ADNTimer *myTimer;

- (id)initWithAdUnitId:(NSString *)editorId_ size:(CGRect)rect;
- (id)initWithAdUnitId:(NSString *)editorId_ size:(CGRect)rect andLocation:(CLLocation *) clientLocation;
- (void) loadAd;

@property (nonatomic, weak) id<ADNDelegate> delegate;
@property (nonatomic, strong) UIViewController *controller;

@end
