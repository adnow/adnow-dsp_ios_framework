//
//  ADNNativeAdRequestTargeting.h
//  Copyright (c) 2015 Adnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLLocation;

/**
 * The `ADNativeAdRequestTargeting` class is used to attach targeting information to
 * `ADNativeAdRequest` objects.
 */

@interface ADNNativeAdRequestTargeting : NSObject

/** @name Creating a Targeting Object */

/**
 * Creates and returns an empty ADNNativeAdRequestTargeting object.
 *
 * @return A newly initialized ADNNativeAdRequestTargeting object.
 */
+ (ADNNativeAdRequestTargeting *)targeting;

/** @name Targeting Parameters */

/**
 * A string representing a set of keywords that should be passed to the Adnow ad server to receive
 * more relevant advertising.
 *
 * Keywords are typically used to target ad campaigns at specific user segments. They should be
 * formatted as comma-separated key-value pairs (e.g. "marital:single,age:24").
 *
 * On the Adnow website, keyword targeting options can be found under the "Advanced Targeting"
 * section when managing campaigns.
 */
@property (nonatomic, copy) NSString *keywords;


/**
 * A `CLLocation` object representing a user's location that should be passed to the Adnow ad server
 * to receive more relevant advertising.
 */
@property (nonatomic, copy) CLLocation *location;

@end
